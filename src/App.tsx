import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { CounterPage } from './features/counter/CounterPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { UsersPage } from './features/users/UsersPage';
import { Navbar } from './core/components/Navbar';
import { HomePage } from './features/home/HomePage';
import { combineReducers, configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { counterReducers } from './features/counter/store/reducers';
import { usersReducer } from './features/users/store/users.reducer';
import { newsStore } from './features/home/store/news.store';
import { catalogReducers } from './features/catalog/store';
import { httpErrorStore } from './core/store/http-error.store';

// Define the store structure
const rootReducer = combineReducers({
  counter: counterReducers,
  users: usersReducer,
  news: newsStore.reducer,
  catalog: catalogReducers,
  httpError: httpErrorStore.reducer
});

// Create the type of store based on rootReducer
export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch

// Configure Store
export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
});

export type AppThunk = ThunkAction<any, RootState, null, Action<string>>


const App: React.FC = () => {
  return (
    <Provider store={store}>

      <BrowserRouter>
        <Navbar />

        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/counter">
            {/*<PageCounterContainer color="red"/>*/}
            <CounterPage />
          </Route>
          <Route path="/users">
            <UsersPage />
          </Route>
          <Route path="/catalog">
            <CatalogPage />
          </Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;

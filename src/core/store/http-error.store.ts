import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// import { addProduct } from '../../features/catalog/store/products.actions';

export const httpErrorStore = createSlice({
  name: 'httpError',
  initialState: false,
  reducers: {
    setError(state, action: PayloadAction<boolean>) {
      return action.payload;
    }
  },
  extraReducers: {
    'products/add/rejected' : (state, action) => {
      console.log('errr')
      return true;
    },
    /*
    // TO FIX: type error
    [addProduct.fulfilled]  : (state, action) => {
      console.log('errr')
      return true;
    }
    */
  }
})

export const {
  setError
} = httpErrorStore.actions;

import { RootState } from '../../App';

export const selectHttpError = (state: RootState) => state.httpError;

import Axios from 'axios';

import { AppThunk } from '../../../App';
import { Product } from '../model/product';
import { getProductsSuccess, toggleProductVisibility, deleteProductSuccess, addProductSuccess } from './products.store';
import { setError } from '../../../core/store/http-error.store';
import { createAsyncThunk } from '@reduxjs/toolkit';

// This action also handle error
export const getProducts = (): AppThunk => async dispatch => {
  try {
    const response = await Axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(response.data))
    dispatch(setError(false))
  } catch (err) {
    dispatch(setError(true))
  }
};

// This action use createAsyncThunk and return promises
// very useful, so your component can dispatch actions but also listen for the promise result
// dispatch(addProduct(...)).then()
export const addProduct = createAsyncThunk<
  Product,
  Omit<Product, 'id' | 'visibility'>
>(
  'products/add',
  async (
    product,
    {  dispatch, rejectWithValue }
  )  => {
    try {
      const newProduct = await Axios.post<Product>(
        'http://localhost:3001/products',
        { ...product, visibility: false }
      );
      dispatch(addProductSuccess(newProduct.data))
      return newProduct.data as Product;
    } catch (err) {
      return rejectWithValue('qfe')
    }

  }
)

/*
// this is a 'classic' addProduct action
export const addProduct = (
  product: Omit<Product, 'id' | 'visibility'>
): AppThunk => async dispatch => {

  const payload: Omit<Product, 'id'> = {
    ...product,
    visibility: false
  };

  try {
    const newProduct = await Axios.post<Product>('http://localhost:3001/products', payload);
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    return rejectWithValue(err.response.data)
  }
};
*/

export const deleteProduct = (
  id: number
): AppThunk => async dispatch => {
  try {
    await Axios.delete(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    // dispatch error
  }
};

export const toggleProduct = (
  product: Product
): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await Axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibility(response.data))
  } catch (err) {
    // dispatch error
  }
};

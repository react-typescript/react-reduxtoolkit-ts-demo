import { RootState } from '../../../App';
import { Product } from '../model/product';

export const selectProductsList = (state: RootState) => state.catalog.list;
export const selectProductsTotal = (state: RootState) => state.catalog.list.reduce((acc: number, cur: Product) => {
  return acc + cur.price;
}, 0)

export const selectProductsByCat = (id: number) => (state: RootState) =>
  state.catalog.list
    .filter(p => p.categoryID === id);


export const selectCategories = ((state: RootState) => state.catalog.categories)

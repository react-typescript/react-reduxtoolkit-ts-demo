// features/catalog/store/product-categories.store.ts
import { createSlice } from '@reduxjs/toolkit';

export const productCategoriesStore = createSlice({
  name: '[products categories]',
  initialState: [
    { id: 1, name: 'Latticini'},
    { id: 2, name: 'Frutta'},
  ],
  reducers: {}
})

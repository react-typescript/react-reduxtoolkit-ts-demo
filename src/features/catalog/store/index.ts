import { combineReducers } from '@reduxjs/toolkit';
import { productsStore } from './products.store';
import { productCategoriesStore } from './product-categories.store';

export const catalogReducers = combineReducers({
  list: productsStore.reducer,
  categories: productCategoriesStore.reducer
});

import React  from 'react';
import { Product } from '../model/product';

interface ProductsListProps {
  total: number;
  products: Product[];
  toggle: (product: Product) => void
  delete: (productId: number) => void
}

export const ProductsList: React.FC<ProductsListProps> = (props) => {
  const deleteHandler = (event: React.MouseEvent<HTMLElement>, id: number) => {
    event.stopPropagation();
    props.delete(id);
  };

  return (
    <>
      {
        props.products.map((product) => {
          return (
            <li
              className="list-group-item"
              style={{ opacity: product.visibility ? 1 : 0.5}}
              key={product.id}
            >
              <span onClick={() => props.toggle(product)}>
                {
                  product.visibility ?
                    <i className="fa fa-eye" /> :
                    <i className="fa fa-eye-slash" />
                }
              </span>
              <span className="ml-2">{ product.title }</span>

              <div className="pull-right">
                { product.price }
                <i
                  className="fa fa-trash "
                  onClick={(e) => deleteHandler(e, product.id)}
                />
              </div>

            </li>
          )
        })
      }
      <div className="text-center">
        <div className="badge badge-info">Total: € {props.total}</div>
      </div>
    </>
  )
};

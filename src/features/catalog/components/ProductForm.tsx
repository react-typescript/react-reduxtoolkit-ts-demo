import React, { useState } from 'react';
import classnames from 'classnames';
import { Product } from '../model/product';

export type ProductForm = Pick<Product, 'title' | 'categoryID'>;


interface ProductsFormProps {
  categories: Array<{ id: number, name: string}>;
  onSubmit: (product: ProductForm) => void;
}

export const ProductsForms: React.FC<ProductsFormProps> = props => {
  const [formData, setFormData] = useState<ProductForm>({
    categoryID: -1, title: ''
  });
  const isTitleValid = formData.title.length > 3
  const isCategoryValid = formData.categoryID !== -1;
  const valid = isTitleValid && isCategoryValid;

  const onChangeHandler = (e: React.FormEvent<HTMLInputElement | HTMLSelectElement>) => {
    setFormData({
      ...formData,
      [e.currentTarget.name]: e.currentTarget.value
    });
  };
  const onSubmitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (valid) {
      props.onSubmit(formData);
      setFormData({categoryID: -1, title: ''});
    }
  };

  return (
    <div className="example">
      <form onSubmit={onSubmitHandler}>
        <input
          className={classnames(
            'form-control',
            { 'is-valid': isTitleValid },
            { 'is-invalid': !isTitleValid },
          )}
          type="text"
          placeholder="Write something..."
          onChange={onChangeHandler}
          name="title"
          value={formData.title}
        />
        <select
          className={classnames(
            'form-control',
            { 'is-valid': isCategoryValid },
            { 'is-invalid': !isCategoryValid },
          )}
          name="categoryID"
          value={formData.categoryID}
          onChange={onChangeHandler}
        >
          <option value={-1}>Select a category</option>
          {
            props.categories.map(c => {
              return <option key={c.id} value={c.id}>{c.name}</option>
            })
          }
        </select>

        <button type="submit" disabled={!valid}>SAVE</button>

      </form>
    </div>
  );
};

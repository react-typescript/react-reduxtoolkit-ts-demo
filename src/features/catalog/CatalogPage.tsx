import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';

import { Product } from './model/product';
import { ProductForm, ProductsForms } from './components/ProductForm';
import { ProductsList } from './components/ProductsList';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products.actions';
import { selectCategories, selectProductsByCat, selectProductsTotal } from './store/products.selectors';
import { selectHttpError } from '../../core/store/http-error.selectors';
import { AppDispatch } from '../../App';

export const CatalogPage: React.FC<any> = () => {
  const total = useSelector(selectProductsTotal);
  const categories = useSelector(selectCategories);
  const [activeCategoryID, setActiveCategoryID] = useState<number>(categories[0].id);
  const products = useSelector(selectProductsByCat(activeCategoryID));
  const errors = useSelector(selectHttpError);

  // trick to use async thunk (as AppDispatch)
  const dispatch = useDispatch() as AppDispatch;

  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  const addTodoHandler = async (formData: ProductForm) => {
    dispatch(
      addProduct({
        title: formData.title,
        price: 10,
        categoryID: +formData.categoryID
      })
    )
      // this is an example of creationAsyncThunk
      .then(res => console.log('action response', res));

  }

  const deleteHandler = (id: number) => {
    dispatch(deleteProduct(id))
  };

  const toggleHandler = (todo: Product) => {
    dispatch(toggleProduct(todo));
  };

  return <div>
    { errors &&  <div className="alert alert-danger">server side error</div>}

    <div>PRODUCT: {products.length}</div>

    <hr/>
    {
      // FILTER CATEGORY
      categories.map(c => {
        return (
          <button
            key={c.id}
            className={classNames(
              'btn',
              { 'btn-primary': c.id === activeCategoryID },
              { 'btn-outline-primary': c.id !== activeCategoryID}
            )}
            onClick={() => setActiveCategoryID(c.id)}
          >
            {c.name}
          </button>
        )
      })
    }

    <ProductsForms onSubmit={addTodoHandler} categories={categories} />

    <ProductsList
      total={total}
      products={products}
      toggle={toggleHandler}
      delete={deleteHandler}
    />
  </div>
};

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { decrement, increment, reset } from './store/actions/counter.actions';
import { setConfig } from './store/actions/config.actions';

export const CounterPage: React.FC = () => {
  const counter = useSelector((state: RootState) => state.counter.value);
  const counterPallet = useSelector((state: RootState) => {
    return Math.ceil(counter / state.counter.config.pallet)
  });
  const dispatch = useDispatch();

  return <div>
    Counter: {counter} - {counterPallet} Pallets required

    <hr/>
    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(10))}>-</button>
    <button onClick={() => dispatch(reset())}>reset</button>
    <hr/>
    <button onClick={() => dispatch(setConfig({ pallet: 5}))}>Pallet of 5 items</button>
    <button onClick={() => dispatch(setConfig({ pallet: 12}))}>Pallet of 12 items</button>
  </div>
};

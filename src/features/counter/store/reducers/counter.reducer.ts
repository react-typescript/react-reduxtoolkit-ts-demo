import { decrement, increment, reset } from '../actions/counter.actions';
import { createReducer, PayloadAction } from '@reduxjs/toolkit';

export const counterReducer = createReducer(0, {
  [increment.type]: (state, action: PayloadAction<number>) => {
    return state + action.payload;
  },
  [decrement.type]: (state, action: PayloadAction<number>) => {
    return state - action.payload;
  },
  [reset.type]: (state, action) => {
    return 0
  },
});

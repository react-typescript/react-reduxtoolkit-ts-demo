// Selectors
import { RootState } from '../../../App';

export const getUsers = (state: RootState) => state.users;
export const getAdmins = (state: RootState) => state.users.filter(t => t.isAdmin);

// users/store/users.actions
import { createAction } from '@reduxjs/toolkit';
import { User } from '../model/user';

export const addUser = createAction<User>('[Users Page] add');
export const deleteUser = createAction<number>('[Users Page] delete');
export const setAsAdmin = createAction<User>('[Users Page] set user as admin');

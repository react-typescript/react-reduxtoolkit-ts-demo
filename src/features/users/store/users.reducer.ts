// users/store/users.reducer
import { User } from '../model/user';
import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { addUser, deleteUser, setAsAdmin } from './users.actions';

const INITIAL_STATE: User[] = [];

export const usersReducer = createReducer(INITIAL_STATE, {
  [addUser.type]: (state, action: PayloadAction<User>) => {
    return [...state, action.payload];
  },
  [deleteUser.type]: (state, action: PayloadAction<number>) => {
    return state.filter(u => u.id !== action.payload);
  },
  [setAsAdmin.type]: (state, action: PayloadAction<User>) => {
    return state.map(u => u.id === action.payload.id ? {...u, isAdmin: !u.isAdmin} : u )
  },
});

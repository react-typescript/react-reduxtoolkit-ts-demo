import React  from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addUser, deleteUser, setAsAdmin } from './store/users.actions';
import { getAdmins, getUsers } from './store/users.selectors';

export const UsersPage: React.FC<any> = props => {
  const users = useSelector(getUsers);
  const admins = useSelector(getAdmins);
  const dispatch = useDispatch();

  const addUserHandler = () => {
    const id = Date.now();
    dispatch(addUser({
      id,
      name: 'User ' + id,
      isAdmin: false
    }))
  };

  return <div>
    <div>Total Users: {users.length}</div>
    <div>Total admins: {admins.length}</div>
    <hr/>
    <button onClick={addUserHandler}>ADD USER</button>

    {
      users.map((u) => {
        return (
          <li className="list-group-item" key={u.id}>
            <span onClick={() => dispatch(setAsAdmin(u))}>
              {
                u.isAdmin ?
                  <i className="fa fa-check" /> :
                  <i className="fa fa-square-o" />
              }
            </span>
            <span className="ml-2"> { u.name }</span>

            <i
              className="fa fa-trash pull-right"
              onClick={() => dispatch(deleteUser(u.id))}
            />

          </li>
        )
      })
    }
  </div>
};

// home/model/news.ts
export interface News {
  id: number;
  title: string;
  published: boolean;
}

// home/HomePage.tsxù
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { addNews, toggleNews } from './store/news.store';

export const HomePage: React.FC = () => {
  const [showPublished, setShowPublished] = useState<string>('all');
  const newsList = useSelector((state: RootState) => {
    switch(showPublished) {
      case 'published':    return state.news.filter(n => n.published);
      case 'unpublished':    return state.news.filter(n => !n.published);
      default: return state.news
    }
  })
  const dispatch = useDispatch();

  function onChange(e: React.KeyboardEvent<HTMLInputElement>) {
    if (e.key === 'Enter') {
      dispatch(addNews(e.currentTarget.value))
      e.currentTarget.value = '';
    }
  }

  function visibilityFilter(e: React.ChangeEvent<HTMLSelectElement>) {
    setShowPublished(e.currentTarget.value)
  }

  return <div>
    <input type="text" onKeyPress={onChange}/>

    <select onChange={visibilityFilter}>
      <option value="all">Show all</option>
      <option value="published">Published only</option>
      <option value="unpublished">Unpublished only</option>
    </select>

    {
      newsList.map(news => {
        return (
          <li className="list-group-item" key={news.id}>
            {news.title}

            <div
              className="badge badge-dark ml-3"
              onClick={() => dispatch(toggleNews(news.id))}
            >
              { news.published ? 'PUBLISHED' : 'UNPUBLISHED'}
            </div>
          </li>
        )
      })
    }
  </div>
};
